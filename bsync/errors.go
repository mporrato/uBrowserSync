package bsync

import (
	"fmt"
	"net/http"
)

type ErrorPayload struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

type SyncError struct {
	StatusCode int
	Payload    ErrorPayload
}

func (e SyncError) Error() string {
	return fmt.Sprintf("code: %s, message: %s", e.Payload.Code, e.Payload.Message)
}

func NewSyncError(code string, message string, status int) SyncError {
	return SyncError{
		StatusCode: status,
		Payload: ErrorPayload{
			Code:    code,
			Message: message}}
}

var (
	NotImplementedError = NewSyncError(
		"NotImplementedException",
		"The requested route has not been implemented",
		http.StatusNotFound)

	MethodNotImplementedError = NewSyncError(
		"NotImplementedException",
		"The requested method has not been implemented",
		http.StatusMethodNotAllowed)

	SyncNotFoundError = NewSyncError(
		"SyncNotFoundException",
		"Sync does not exist",
		http.StatusUnauthorized)

	SyncConflictError = NewSyncError(
		"SyncConflictException",
		"A sync conflict was detected",
		http.StatusConflict)

	SyncDataLimitExceededError = NewSyncError(
		"SyncDataLimitExceededException",
		"Sync data limit exceeded",
		http.StatusRequestEntityTooLarge)

	RequiredDataNotFoundError = NewSyncError(
		"RequiredDataNotFoundException",
		"Unable to find required data",
		http.StatusBadRequest)

	NewSyncsForbiddenError = NewSyncError(
		"NewSyncsForbiddenException",
		"The service is not accepting new syncs",
		http.StatusMethodNotAllowed)
)
