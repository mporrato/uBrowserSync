package bsync

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

type FSStore struct {
	path  string
	count int
}

func NewFSStore(path string) (*FSStore, error) {
	p, err := filepath.Abs(path)
	if err != nil {
		return nil, err
	}
	err = os.MkdirAll(p, 0700)
	if err != nil {
		return nil, err
	}
	var s = new(FSStore)
	s.path = p
	s.updateFileCount()
	return s, nil
}

func (drv *FSStore) updateFileCount() {
	drv.count = 0
	filepath.Walk(drv.path, func(path string, info fs.FileInfo, err error) error {
		if !info.IsDir() && strings.HasSuffix(info.Name(), ".json") {
			drv.count++
		}
		return nil
	})
}

func (drv *FSStore) storePath(id string) string {
	return filepath.Join(drv.path, id[0:3], fmt.Sprintf("%s.json", id))
}

func (drv *FSStore) RawSave(s *Blob) error {
	body, err := json.Marshal(s)
	if err != nil {
		return err
	}
	fileName := drv.storePath(s.ID)
	dirName := filepath.Dir(fileName)
	_, err = os.Stat(fileName)
	if err != nil {
		err = os.Mkdir(dirName, 0700)
		if err != nil && !os.IsExist(err) {
			return err
		}
		drv.count++
	}
	f, err := os.CreateTemp(dirName, "tmp-"+s.ID+".*")
	if err != nil {
		return err
	}
	tempName := f.Name()
	_, err = f.Write(body)
	err2 := f.Close()
	if err != nil {
		_ = os.Remove(f.Name())
		return err
	}
	if err2 != nil {
		return err2
	}
	return os.Rename(tempName, fileName)
}

func (drv *FSStore) RawLoad(id string) (*Blob, error) {
	body, err := ioutil.ReadFile(drv.storePath(id))
	if err != nil {
		if os.IsNotExist(err) {
			return nil, SyncNotFoundError
		}
		return nil, err
	}
	s := new(Blob)
	err = json.Unmarshal(body, s)
	return s, err
}

func (drv *FSStore) Exists(id string) bool {
	st, err := os.Stat(drv.storePath(id))
	if err != nil {
		return false
	}
	return (!st.IsDir()) && (st.Size() > 0)
}

func (drv *FSStore) Count() int {
	return drv.count
}
