package bsync

import "time"

type Blob struct {
	ID           string    `json:"id"`
	Bookmarks    string    `json:"bookmarks"`
	Version      string    `json:"version"`
	Created      time.Time `json:"created"`
	LastUpdated  time.Time `json:"lastUpdated"`
	LastAccessed time.Time `json:"lastAccessed"`
}

type CreateResp struct {
	ID          string    `json:"id"`
	LastUpdated time.Time `json:"lastUpdated"`
	Version     string    `json:"version"`
}

type GetResp struct {
	Bookmarks   string    `json:"bookmarks"`
	LastUpdated time.Time `json:"lastUpdated"`
	Version     string    `json:"version"`
}

type ServiceInfoResp struct {
	Version     string `json:"version"`
	Message     string `json:"message"`
	MaxSyncSize int    `json:"maxSyncSize"`
	Status      int    `json:"status"`
}

type CreateReq struct {
	Version string `json:"version"`
}

type UpdateReq struct {
	Bookmarks   string    `json:"bookmarks"`
	LastUpdated time.Time `json:"lastUpdated"`
}

type UpdateResp struct {
	LastUpdated time.Time `json:"lastUpdated"`
}

type LastUpdatedResp struct {
	LastUpdated time.Time `json:"lastUpdated"`
}

type GetSyncVerResp struct {
	Version string `json:"version"`
}
