package bsync

type MemStore map[string]Blob

func NewMemStore() (*MemStore, error) {
	r := make(MemStore)
	return &r, nil
}

func (drv *MemStore) RawSave(s *Blob) error {
	(*drv)[s.ID] = *s
	return nil
}

func (drv *MemStore) RawLoad(id string) (*Blob, error) {
	r, e := (*drv)[id]
	if !e {
		return nil, SyncNotFoundError
	}
	return &r, nil
}

func (drv *MemStore) Exists(id string) bool {
	_, r := (*drv)[id]
	return r
}

func (drv *MemStore) Count() int {
	return len(*drv)
}
