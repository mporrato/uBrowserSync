package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strings"
	"time"

	"gitlab.com/mporrato/uBrowserSync/bsync"
)

const (
	apiVersion         = "1.1.13"
	infoMessage        = "Powered by uBrowserSync"
	defaultMaxSyncSize = 512000
	defaultMaxSyncs    = 10000
)

const (
	statusOnline   = 1
	statusOffline  = 2
	statusReadOnly = 3
)

var (
	config struct {
		listen      string
		store       string
		maxSyncSize int
		maxSyncs    int
	}

	store         bsync.Store
	serviceStatus = statusOnline
	sidRe         = regexp.MustCompile("^[[:xdigit:]]{32}$")

	invalidRequestError = bsync.NewSyncError(
		"Invalid request",
		"Malformed request body",
		http.StatusBadRequest)
)

func sendJSON(w http.ResponseWriter, status int, data interface{}) {
	body, err := json.Marshal(data)
	if err != nil {
		log.Printf("sendJSON(%v): json.Marshal() failed: %v", data, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	_, err = w.Write(body)
	if err != nil {
		log.Println("sendJSON() failed:", err)
	}
}

func sendJSONOk(w http.ResponseWriter, data interface{}) {
	sendJSON(w, http.StatusOK, data)
}

func getSyncId(req *http.Request) (string, error) {
	syncId := req.PathValue("id")
	if !sidRe.MatchString(syncId) {
		return "", bsync.SyncNotFoundError
	}
	return syncId, nil
}

func sendJSONError(w http.ResponseWriter, err error) {
	log.Println("ERROR: ", err)
	switch e := err.(type) {
	case bsync.SyncError:
		sendJSON(w, e.StatusCode, e.Payload)
	default:
		sendJSON(w, http.StatusInternalServerError,
			bsync.ErrorPayload{
				Code:    "Internal server error",
				Message: e.Error()})
	}
}

func getInfo(w http.ResponseWriter, req *http.Request) {
	log.Println("getInfo()")
	serviceInfo := bsync.ServiceInfoResp{
		Version:     apiVersion,
		Message:     infoMessage,
		Status:      serviceStatus,
		MaxSyncSize: config.maxSyncSize}
	sendJSONOk(w, serviceInfo)
}

func ensureJSONRequest(w http.ResponseWriter, req *http.Request) bool {
	contentType := strings.Split(strings.ToLower(req.Header["Content-Type"][0]), ";")[0]
	if contentType != "application/json" {
		sendJSONError(w, bsync.RequiredDataNotFoundError)
		return false
	}
	return true
}

func createSync(w http.ResponseWriter, req *http.Request) {
	log.Println("createSync()")
	if serviceStatus != statusOnline {
		sendJSONError(w, bsync.NewSyncsForbiddenError)
		return
	}
	if !ensureJSONRequest(w, req) {
		return
	}
	body := new(bsync.CreateReq)
	req.Body = http.MaxBytesReader(w, req.Body, 10000)
	err := json.NewDecoder(req.Body).Decode(&body)
	if err != nil {
		sendJSONError(w, invalidRequestError)
		return
	}
	if len(body.Version) > 20 {
		sendJSONError(w, invalidRequestError)
		return
	}
	resp, err := store.Create(body.Version)
	if err != nil {
		sendJSONError(w, err)
		return
	}
	if config.maxSyncs > 0 && store.Count() >= config.maxSyncs {
		serviceStatus = statusReadOnly
	}
	log.Printf("Sync %s created\n", resp.ID)
	sendJSONOk(w, resp)
}

func getSync(w http.ResponseWriter, req *http.Request) {
	syncId, err := getSyncId(req)
	if err != nil {
		sendJSONError(w, err)
		return
	}
	log.Printf("getSync(%s)", syncId)
	resp, err := store.Get(syncId)
	if err != nil {
		sendJSONError(w, err)
		return
	}
	sendJSONOk(w, resp)
}

func updateSync(w http.ResponseWriter, req *http.Request) {
	syncId, err := getSyncId(req)
	if err != nil {
		sendJSONError(w, err)
		return
	}
	log.Printf("updateSync(%s)", syncId)
	if !ensureJSONRequest(w, req) {
		return
	}
	body := new(bsync.UpdateReq)
	req.Body = http.MaxBytesReader(w, req.Body, int64(10000+config.maxSyncSize))
	err = json.NewDecoder(req.Body).Decode(&body)
	if err != nil {
		sendJSONError(w, invalidRequestError)
		return
	}
	if len(body.Bookmarks) > config.maxSyncSize {
		sendJSONError(w, bsync.SyncDataLimitExceededError)
		return
	}
	resp, err := store.Update(syncId, body.Bookmarks, body.LastUpdated)
	if err != nil {
		sendJSONError(w, err)
		return
	}
	sendJSONOk(w, bsync.UpdateResp{LastUpdated: resp})
}

func getSyncLastUpdated(w http.ResponseWriter, req *http.Request) {
	syncId, err := getSyncId(req)
	if err != nil {
		sendJSONError(w, err)
		return
	}
	log.Printf("getLastUpdated(%s)", syncId)
	resp, err := store.GetLastUpdated(syncId)
	if err != nil {
		sendJSONError(w, err)
		return
	}
	sendJSONOk(w, bsync.LastUpdatedResp{LastUpdated: resp})
}

func getSyncVersion(w http.ResponseWriter, req *http.Request) {
	syncId, err := getSyncId(req)
	if err != nil {
		sendJSONError(w, err)
		return
	}
	log.Printf("getVersion(%s)", syncId)
	resp, err := store.GetVersion(syncId)
	if err != nil {
		sendJSONError(w, err)
		return
	}
	sendJSONOk(w, bsync.GetSyncVerResp{Version: resp})
}

func notFound(w http.ResponseWriter, _ *http.Request) {
	sendJSONError(w, bsync.NotImplementedError)
}

func init() {
	var (
		err      error
		storeDrv bsync.StoreDriver
	)

	flag.StringVar(&config.listen, "listen", ":8090", "listen address and port")
	flag.StringVar(&config.store, "store", "fs:data", "blob store driver")
	flag.IntVar(&config.maxSyncSize, "maxsize", defaultMaxSyncSize, "maximum size of a sync in bytes")
	flag.IntVar(&config.maxSyncs, "maxsyncs", defaultMaxSyncs, "maximum number of syncs")
	flag.Parse()

	storeFlagTokens := strings.Split(config.store, ":")

	switch storeFlagTokens[0] {
	case "fs":
		if len(storeFlagTokens) != 2 {
			err = fmt.Errorf("argument required for fs store driver")
		} else {
			storeDrv, err = bsync.NewFSStore(storeFlagTokens[1])
		}
	case "mem":
		storeDrv, err = bsync.NewMemStore()
	default:
		err = fmt.Errorf("Invalid store driver: " + storeFlagTokens[0])
	}
	if err != nil {
		log.Fatalf("store initialization failed: %v", err)
	}
	store = bsync.NewStore(storeDrv)
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", notFound)
	mux.HandleFunc("GET /info", getInfo)
	mux.HandleFunc("POST /bookmarks", createSync)
	mux.HandleFunc("GET /bookmarks/{id}", getSync)
	mux.HandleFunc("PUT /bookmarks/{id}", updateSync)
	mux.HandleFunc("GET /bookmarks/{id}/lastUpdated", getSyncLastUpdated)
	mux.HandleFunc("GET /bookmarks/{id}/version", getSyncVersion)

	log.Println("HTTP server listening on", config.listen)
	server := &http.Server{
		Addr:           config.listen,
		Handler:        mux,
		ReadTimeout:    15 * time.Second,
		WriteTimeout:   15 * time.Second,
		MaxHeaderBytes: 5000}
	err := server.ListenAndServe()
	log.Println("HTTP server terminated", err)
}
