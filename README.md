# uBrowserSync

The _uncomplicated browser sync server_ is an implementation of the [xbrowsersync](https://www.xbrowsersync.org/) server API with no external dependencies.

Being written in Go, the server is a standalone statically linked binary; it uses the local filesystem as the datastore
by default and can be built for any operating system and hardware platform supported by the Go compiler, making it ideal
for resource constrained self-hosting scenarios.

## Why

xBrowserSync is a great tool for privacy minded people. The problem with the official server code is about the
software stack. It's written in NodeJS and requires MongoDB for storing data. I don't use NodeJS or MongoDB for anything
else on my VPS so adding a self-hosted xBrowserSync API server would pull in a lot of code and require a lot of
additional resources.

The xBrowserSync API is simple enough to make writing a new server an almost trivial exercise. This was the perfect
opportunity for me to improve my Go programming skills. I'm still very far from being an expert Go developer but it was
worth my time.

## How

### Quickstart

To run a test service locally on your machine:

```shell
mkdir $HOME/ubs-data
docker run -d -v $HOME/ubs-data:/data -p 9999:8090 registry.gitlab.com/mporrato/ubrowsersync/ubsserver
```

Then point the xBrowserSync browser extension to `http://127.0.0.1:9999`.

### Building

You can get prebuilt binaries for a number of platforms from the gitlab repository in the "Download artifacts" section.
To build from source, just run:

`go install ./cmd/ubsserver`

### Running

To run the binary you just built:

`~/go/bin/ubsserver`

It will start the API server listening on port 8090 and using the `data` directory under $PWD as the datastore.

There are a few command line flags that can be used to change the behaviour of the API service:

 - `-listen $address:$port`: changes the listening address of the built-in HTTP server, for example `-listen :9999` will
   listen on port 9999 on all addresses, while `-listen 127.0.0.1:9876` will only listen on port 9876 on the loopback
   interface
 - `-store $driver_name:$driver_args`: changes the datastore driver and its configuration. `$driver_name` is the name of
   the store driver while `$driver_args` contains driver-specific settings. Currently, only two drivers are implemented:
   - `mem`: a volatile RAM-backed datastore only useful for testing and debugging. It does not take any arguments
     (i.e.: it can be only used by specifying `-store mem`).
   - `fs`: the default driver: it stores sync data in json files in the directory specified as the argument (default is
     `data`)
  - `-maxsize $size`: changes the maximum size of a sync (in bytes) that can be accepted by the API. The default is set
    to 512000.
  - `-maxsyncs $num`: change the maximum number of syncs that can be stored. Default is 10000.

## Roadmap

There are a few missing features that I would like to add.

Medium priority:
 - **Rate limiting**: this can be partially enforced by a reverse proxy in front of the API server but would be nice to
   have it as part of the server itself

Nice to have:
 - **Pruning**: syncs never expire; either a background job or a standalone tool is needed to make cleanup operations
   simpler
 - **Front page**: serving a static HTML page on `/`, perhaps with some simple JS to show the service status, should be
   fairly straightforward and would improve the end user experience. The main blocker here is my lack of design skills
   to craft a half decent looking front page
 - **Migration tool**: a tool able to pull your current sync from a server and push it to a different one might come
   handy; the main hurdle is that it would require decrypting the payload and re-encrypting it because the sync ID is
   used as the IV in the AES-GCM encryption process
   
## License

The code is released under the "Unlicense" public domain license. See [UNLICENSE](UNLICENSE) for more details.